package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by alejandrobarba on 24/11/15.
 */
public class AnalizadorSemantico {

    public static Arbol arbolSintactico;
    private List<Token> tokens = new ArrayList<Token>();
    private List<String> lista = new ArrayList<String>();
    private List<TabSim> tabSim = new ArrayList<TabSim>();
    private List<TabError> tabError = new ArrayList<TabError>();
    private boolean valido = true;
    private boolean actTabSim = false;
    private int cont = 0;
    private int pos;
    private int indice = 0;

    public static ArrayList<TabSim> tb = new ArrayList<TabSim>();

    public AnalizadorSemantico() {
        lista.add("if");
        lista.add("else");
        lista.add("while");
        lista.add("end");
    }

    private boolean buscaToken(String token) {
        if (lista.contains(token)) {
            return true;
        }
        return false;
    }

    private int busquedaSimbolo(String token) {
        int indice = -1;
        //Buscar en el tabSim
        for (int i = 0; i < tabSim.size(); i++) {
            if (tabSim.get(i).getToken().getToken().equals(token)) {
                indice = i;
            }
        }
        return indice;
    }

    private void comprueba(int tipo) {
        if (tokens.get(cont).getTipo() == tipo) {
            cont++;
        } else {
            //Que pedo ?
            System.out.println("Error comprueba");
            valido = false;
        }
    }

    private void asignamosValor(int tipo) {

        switch (tipo) {
            case Constantes.INT:
                if (actTabSim) {
                    // Actualizamos el simbolo con su nuevo valor con la posicion de la busqueda anterior
                    tabSim.get(pos).setValor(Integer.parseInt(tokens.get(cont).getToken()));
                } else {
                    // Ingresamoe el valor para el simbolo
                    tabSim.get(indice).setValor(Integer.parseInt(tokens.get(cont).getToken()));
                    // Movemos el indice de simbolos
                    indice++;
                }
                break;

            case Constantes.FLOAT:
                if (actTabSim) {
                    // Actualizamos el simbolo con su nuevo valor con la posicion de la busqueda anterior
                    tabSim.get(pos).setValor(Float.parseFloat(tokens.get(cont).getToken()));
                } else {
                    // Ingresamoe el valor para el simbolo
                    tabSim.get(indice).setValor(Float.parseFloat(tokens.get(cont).getToken()));
                    // Movemos el indice de simbolos
                    indice++;
                }
                break;

            case Constantes.IDENTIFICADOR:

                // Salvamos la posicion del simbolo que ya esta en la tabla ( Si es el caso)
                int tmp = pos;

                // Realizamos la posicion del elemento al cual se hace referencia en la asignacion
                pos = busquedaSimbolo(tokens.get(cont).getToken());

                if (pos >= 0){
                    if(actTabSim){
                        // Actualizamos el registro existente con la referencia al otro simbolo
                        tabSim.get(tmp).setValor(tabSim.get(pos).getValor());
                    }else {
                        // Asignamos el valor al con la referencia al otro simbolo
                        tabSim.get(indice).setValor(tabSim.get(pos).getValor());
                        indice++;
                    }
                }
                else {
                    // No existe la referencia
                    valido = false;
                    tabError.add(new TabError(tokens.get(cont).getToken(),Constantes.NO_REFERENCIA,cont));
                }
                break;
        }
    }


    public boolean analiza() {
        recorridoInOrden(arbolSintactico.getRaiz());
        System.out.print("\nTabla de simbolos\n\n");
        while (valido && cont < tokens.size() - 1) {
            // Si es < IDE > pero no es palabra reservada
            if (tokens.get(cont).getTipo() == Constantes.IDENTIFICADOR && !buscaToken(tokens.get(cont).getToken())) {

                pos = -1;
                actTabSim = false;

                // Buscamos si el token no esta ya en la tabla de simbolos
                pos = busquedaSimbolo(tokens.get(cont).getToken());
                if (pos < 0) {
                    // Si no esta agregamos el token a la tabla
                    tabSim.add(indice, new TabSim(tokens.get(cont)));
                    //indice++;
                } else {
                    actTabSim = true;
                }
                // Consumimos el token
                cont++;

                // Consumimos < = >
                comprueba(Constantes.ASIGNACION);

                // Asignamos valor a los simbolos
                asignamosValor(tokens.get(cont).getTipo());

                // Consumimos el token
                cont++;

            } else {
                //Es < IDE > y palabra reservada o alguna otra cosa rara
                cont++;
            }
        }

        /*
            Recorrido del tab sim
         */
        for (int i = 0; i < tabSim.size(); i++) {
            System.out.println(tabSim.get(i).getToken().getToken() + " | " + tabSim.get(i).getValor());
        }

        // Lista de errores del analisis semantico
        if (!valido && tabError.size() > 0) {
            System.out.println("\n\n****ERRORES analisis semantico****");
            for (int i = 0; i < tabError.size(); i++) {
                System.out.println("Error con el token " +
                        tabError.get(i).getPos_token() +
                        ": '" + tabError.get(i).getConcepto() +
                        "' debido a que " + tabError.get(i).getTipo_esperado());
            }
            System.out.println();
        }


        return valido;
    }

    public void recorridoInOrden(Nodo raiz) {
        if (raiz != null) {
            recorridoInOrden(raiz.getHojaIzquierda());
            //System.out.print(raiz.getValor().getToken() + " ");
            tokens.add(raiz.getValor());
            recorridoInOrden(raiz.getHojaDerecha());
        }
    }

}
