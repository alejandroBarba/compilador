package com.company;

import java.io.*;

/**
 * Created by alejandrobarba on 29/08/15.
 */
public class FileControl {

    public BufferedReader initR(String name){
        BufferedReader br = null;
        String InputFile = name;
        try {
            File arch = new File(InputFile);
            FileReader fr = new FileReader(arch);
            br = new BufferedReader(fr);
        }catch (IOException e){e.printStackTrace();}
        return br;
    }

    public BufferedWriter initW(String name){
        BufferedWriter bw = null;
        String OutputFile = name;
        try {
            File arch = new File(OutputFile);
            FileWriter fw = new FileWriter(arch);
            bw = new BufferedWriter(fw);
        }catch (IOException e){e.printStackTrace();}
        return bw;
    }

    public String readFile(BufferedReader br){
        String line = null;
        try {
            line = br.readLine();
        }catch (IOException e){e.printStackTrace();}
        //line = line.replaceAll("\\s"," ");
        return line;
    }

    public boolean writeFile(BufferedWriter bw, String token){
        try {
            bw.append(token);
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean close(BufferedReader br, BufferedWriter bw){
        try {
            br.close();
            bw.close();
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean closeW(BufferedWriter bw){
        try {
            bw.close();
            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean delete(String nameFile){
        File file = new File(nameFile);
        if(file != null) {
            file.delete();
            return true;
        }else
            return false;
    }
}
