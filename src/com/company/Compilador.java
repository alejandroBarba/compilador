package com.company;

import java.io.BufferedReader;
import java.util.ArrayList;

public class Compilador {

    //El analizador sintactico manda llamar al analizador lexico.
    AnalizadorSintactico sintactico = new AnalizadorSintactico();
    AnalizadorSemantico semantico = new AnalizadorSemantico();
    GeneradorCodigo generar = new GeneradorCodigo();

    public void compila() {
        if (sintactico.analiza()) {
            System.out.println("Sintactico: OK");
            if (semantico.analiza()) {
                //System.out.println("Semantico: 1");
                generar.genera();
            } else {
                System.out.print("Semantico: ERROR");
            }
        } else {
            if (!sintactico.error_lexico)
                System.out.print("Sintactico: ERROR");
        }

    }

    public static void main(String[] args) {
        Compilador compilador = new Compilador();
        compilador.compila();
    }
}
