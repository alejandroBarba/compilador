package com.company;

import java.io.BufferedWriter;

/**
 * Created by alejandrobarba on 24/11/15.
 */
public class GeneradorCodigo {
    public static Arbol arbolSintactico;
    FileControl file = new FileControl();
    BufferedWriter bw;

    public void genera(){
        bw = file.initW("ensamblador.txt");
        file.writeFile(bw,".386\n");
        file.writeFile(bw,"" +
                "option casemap:none ;labels are case-sensitive now\n" +
                "\n" +
                "    include \\masm32\\macros\\macros.asm       \n" +
                "    include \\masm32\\include\\masm32.inc\n" +
                "    include \\masm32\\include\\kernel32.inc\n" +
                "\n" +
                "    includelib \\masm32\\lib\\masm32.lib\n" +
                "    includelib \\masm32\\lib\\kernel32.lib\n\n");

        file.writeFile(bw,".data\n");
        file.writeFile(bw,".data?\n");

        if(AnalizadorSemantico.tb.size() > 0){
            for (int i = 0; i < AnalizadorSemantico.tb.size();i++){
                file.writeFile(bw,"\t" + AnalizadorSemantico.tb.get(i).getToken().getToken() + " dword ?\n");
            }
        }

        file.writeFile(bw,".code\n");
        file.writeFile(bw,"Inicio:\n");
        //Do somenthing
        file.writeFile(bw,"\texit\n");
        file.writeFile(bw,"end Inicio");
        file.closeW(bw);
    }
}
