package com.company;

/**
 * Created by alejandrobarba on 30/04/16.
 */
public class TabError {
    private String token_error;
    private int tipo_esperado;
    private int pos_token;

    public TabError(String concepto, int tipo_esperado, int pos_token) {
        this.tipo_esperado = tipo_esperado;
        this.token_error = concepto;
        this.pos_token = pos_token + 1;
    }

    public String getConcepto() {
        return token_error;
    }

    public String getTipo_esperado() {
        String concepto = "null";
        switch (tipo_esperado) {
            case Constantes.IDENTIFICADOR:
                concepto = "identificador";
                break;
            case Constantes.OP_ARITMETICOS:
                concepto = "operador aritmetico";
                break;
            case Constantes.OP_RELACIONAL:
                concepto = "operador relacional";
                break;
            case Constantes.SALTO:
                concepto = "salto de linea";
                break;
            case Constantes.END:
                concepto = "clausula END";
                break;
            case Constantes.INT:
                concepto = "numero";
                break;
            case Constantes.NO_REFERENCIA:
                concepto = "no existe la variable";
                break;
            case Constantes.$:
                concepto = "caracter de fin de archivo";
                break;
            default:
                concepto = "tipo no definido: " + tipo_esperado;
                break;
        }
        return concepto;
    }

    public int getPos_token() {
        return pos_token;
    }


}
