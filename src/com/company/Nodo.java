package com.company;

/**
 * Created by alejandrobarba on 23/11/15.
 */
public class Nodo {

    private Token valor;

    private Nodo padre;
    private Nodo hojaIzquierda;
    private Nodo hojaDerecha;

    /* Constructor */
    public Nodo(Token valor) {
        this.valor = valor;
    }

    public Nodo(){}

    /* Setters y Getters */
    public void setValor(Token valor) {
        this.valor = valor;
    }

    public Token getValor() {
        return valor;
    }

    public Nodo getPadre() {
        return padre;
    }

    public void setPadre(Nodo padre) {
        this.padre = padre;
    }

    public Nodo getHojaIzquierda() {
        return hojaIzquierda;
    }

    public void setHojaIzquierda(Nodo hojaIzquierda) {
        this.hojaIzquierda = hojaIzquierda;
    }

    public Nodo getHojaDerecha() {
        return hojaDerecha;
    }

    public void setHojaDerecha(Nodo hojaDerecha) {
        this.hojaDerecha = hojaDerecha;
    }

    public static class Asignacion extends Nodo{
        private Nodo ide;
        private Nodo expresion;

        public Asignacion(Nodo ide, Nodo expresion){
            this.ide = ide;
            this.expresion = expresion;
        }
    }

    public static class IDE extends Nodo{
        private Nodo ide;
        public IDE(Nodo ide){
            this.ide = ide;
        }
    }

    public static class Expresion extends Nodo{
        private Nodo expresion;
        public Expresion(Nodo expresion){
            this.expresion = expresion;
        }
    }
}
