package com.company;

/**
 * Created by alejandrobarba on 23/11/15.
 */
public class Arbol {

    /* Atributos */
    private Nodo raiz;

    /* Contructories */
    public Arbol( Token valor ) {

        this.raiz = new Nodo( valor );
    }

    public Arbol( Nodo raiz ) {

        this.raiz = raiz;
    }

    public Arbol(  ) {

    }

    /* Setters y Getters */
    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    //1 - izq
    //2 - der
    private void AgregarNodo( Nodo nodo, Nodo raiz , int lado) {
        if ( raiz == null ) {
            this.setRaiz(nodo);
        }
        else {
            if ( lado == 1 ) {
                if (raiz.getHojaIzquierda() == null) {
                    raiz.setHojaIzquierda(nodo);
                }
                else {
                    AgregarNodo( nodo , raiz.getHojaIzquierda() , 1 );
                }
            }
            else {
                if (raiz.getHojaDerecha() == null) {
                    raiz.setHojaDerecha(nodo);
                }
                else {
                    AgregarNodo( nodo, raiz.getHojaDerecha(),2 );
                }
            }
        }
    }

    public void AgregarNodo( Nodo nodo, int lado ) {
        this.AgregarNodo( nodo , this.raiz, lado );
    }

    public void imprimirEntre (Nodo reco)
    {
        if (reco != null)
        {
            imprimirEntre (reco.getHojaIzquierda());
            System.out.print(reco.getValor().getToken() + " ");
            imprimirEntre (reco.getHojaDerecha());
        }
    }

    public void imprimirPre (Nodo reco)
    {
        if (reco != null)
        {
            System.out.print(reco.getValor().getToken() + " ");
            imprimirPre (reco.getHojaIzquierda());
            imprimirPre (reco.getHojaDerecha());
        }
    }

    public void imprimirPost (Nodo reco)
    {
        if (reco != null)
        {
            imprimirPost (reco.getHojaIzquierda());
            imprimirPost (reco.getHojaDerecha());
            System.out.print(reco.getValor().getToken() + " ");
        }
    }

}
