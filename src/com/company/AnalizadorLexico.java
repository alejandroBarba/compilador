package com.company;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class AnalizadorLexico {

    /**

     */

    //Analizador lexico
    private String cadena = null;
    private List<EstadoValido> lista = new ArrayList<EstadoValido>();
    private int tamañoCadena;
    private int indice;
    private boolean error = false;
    private int estado = 0;
    private int tipo = 0;

    private int[][] automata = {
            //Estado - Simbolo de entrada
            {0, 1, 2, -1, 5, 6, 7, 8, 9, 10, 11, 13, 14, 16, 15},
            {-1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, 2, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, 12, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 18},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 17, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
    };

    public AnalizadorLexico() {
        //estados validos
        lista.add(new EstadoValido(1, 1, 1));//ide 1
        lista.add(new EstadoValido(1, 2, 1));//ide
        lista.add(new EstadoValido(2, 2, 2));//int 2
        lista.add(new EstadoValido(4, 2, 3));//float 3
        lista.add(new EstadoValido(5, 4, 12));//op aritmeticos 2 12
        lista.add(new EstadoValido(6, 5, 4));//op aritmetico 1 4
        lista.add(new EstadoValido(7, 6, 5));//del ; 5
        lista.add(new EstadoValido(8, 7, 6));//agrupacion 1 6
        lista.add(new EstadoValido(9, 8, 7));//agrupacion 2 7
        lista.add(new EstadoValido(10, 9, 8));//agrupacion 3 8
        lista.add(new EstadoValido(11, 10, 9));//op relacional 9
        lista.add(new EstadoValido(12, 11, 9));//op relacional 9
        lista.add(new EstadoValido(12, 12, 12));//op relacional 9
        lista.add(new EstadoValido(13, 11, 10));//asignacion 10
        lista.add(new EstadoValido(14, 11, 9));//op relacional 9
        lista.add(new EstadoValido(17, 13, 11));//op logico 11
        lista.add(new EstadoValido(18, 14, 11));//op logico 11
    }

    private boolean edoAceptacion(int estado, int entrada) {
        boolean bandera = false;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getFila() == estado && lista.get(i).getColumna() == entrada && !bandera) {
                tipo = lista.get(i).getTipo();
                bandera = true;
            }
        }
        return bandera;
    }

    private String formato(String tmp) {
        tmp = tmp.replaceAll("\\s", "");
        tmp = tmp.replaceAll("\\t", "");
        return tmp;
    }

    private int entrada(char caracter) {
        int entrada = -1;

        if (caracter == ' ' || caracter == 9) {
            entrada = 0;
        }
        if ((caracter >= 'A' && caracter <= 'z') || caracter == '_') {
            entrada = 1;
        }
        if (caracter >= '0' && caracter <= '9') {
            entrada = 2;
        }
        if (caracter == '.' || caracter == ',') {
            entrada = 3;
        }
        if (caracter == '*' || caracter == '/') {
            entrada = 4;
        }
        if (caracter == '+' || caracter == '-') {
            entrada = 5;
        }
        if (caracter == ';') {
            entrada = 6;
        }
        if (caracter == '(' || caracter == ')') {
            entrada = 7;
        }
        if (caracter == '[' || caracter == ']') {
            entrada = 8;
        }
        if (caracter == '{' || caracter == '}') {
            entrada = 9;
        }
        if (caracter == '<' || caracter == '>') {
            entrada = 10;
        }
        if (caracter == '=') {
            entrada = 11;
        }
        if (caracter == '~') {
            entrada = 12;
        }
        if (caracter == '|') {
            entrada = 13;
        }
        if (caracter == '&') {
            entrada = 14;
        }
        if (caracter == '\n') {
            entrada = 15;
        }
        return entrada;
    }

    private int siguienteEstado(int entrada) {
        int e;
        e = automata[estado][entrada];
        return e;
    }

    private boolean aceptacion(int e, String s, int i) {
        boolean continua = false;

        if (edoAceptacion(e, entrada(ultimoCaracter(s, i))) && e != 0) {
            estado = 0;
            error = false;
            continua = true;
        }
        return continua;
    }

    private char siguienteCaracter(String s, int i) {
        return s.charAt(i);
    }

    private char ultimoCaracter(String s, int i) {
        char c = 'a';
        try {
            c = s.charAt(i - 1);
        } catch (Exception e) {
        }
        return c;
    }

    public ArrayList<Token> analiza(BufferedReader br) {
        //Variables control
        char c;
        int cont = 0;
        StringBuffer buffer = new StringBuffer();
        ArrayList<Token> token = new ArrayList<Token>();

        //BufferedReader br;
        FileControl file = new FileControl();
        cadena = file.readFile(br);

        //leemos cadena del archivo
        indice = 0;

        while (cadena != null && !error) {
            if (cont != 0) {
                String tmp = formato(cadena);
                if (tmp.length() > 0) {
                    token.add(cont, new Token("salto", 13));
                    cont++;
                }
            }
            indice = 0;
            tamañoCadena = cadena.length();
            while (indice < tamañoCadena && !error) {
                //leemos caracter de la cadena
                c = siguienteCaracter(cadena, indice);

                if (entrada(c) < 0) {
                    //Entrada invalida
                    error = true;
                    System.out.println("Error " + "'" + c + "'" + " simbolo no valido.");
                    //Checamos que este en estado de aceptacion
                    if (aceptacion(estado, cadena, indice)) {
                        token.add(cont, new Token(buffer.toString(), tipo));
                        cont++;
                        //System.out.println("Entrada valida: " + buffer.toString());
                        buffer.delete(0, buffer.length());
                    }
                } else {
                    //Entrada valida
                    if (siguienteEstado(entrada(c)) > 0 && !error) {
                        //Se arma el  token valido
                        buffer.append(c);
                        //Asignamos el siguiente estado
                        estado = siguienteEstado(entrada(c));
                        indice++;
                    } else {
                        //Entrada valida pero no es valida para token
                        //System.out.println(c + " es entrada: " + entrada(c));
                        if (/*entrada(c) != 0 && */aceptacion(estado, cadena, indice)) {
                            token.add(cont, new Token(buffer.toString(), tipo));
                            cont++;
                            buffer.delete(0, buffer.length());
                        } else {
                            if (entrada(c) == 0) {
                                indice++;
                            } else
                                error = true;
                        }
                    }
                }
            }
            //Valida al final de la cadena
            if (!cadena.equals("") && aceptacion(estado, cadena, indice)) {
                token.add(cont, new Token(buffer.toString(), tipo));
                cont++;
                buffer.delete(0, buffer.length());
            }
            cadena = file.readFile(br);
        }
        token.add(cont, new Token("$", -1));
        AnalizadorSintactico.error_lexico = error;
        return token;
    }


}