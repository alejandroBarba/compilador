package com.company;

import javafx.beans.binding.IntegerBinding;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandrobarba on 17/11/15.
 */
public class AnalizadorSintactico {

    //private String cadena;
    private int cont = 0;                                   //Indice de tokens
    private boolean valido = true;                          //Error sintactico
    private ArrayList<Token> tokens;                        //Lista de tokens <- Lexico
    private List<String> lista = new ArrayList<String>();   //Lista palabras reservadas
    private List<TabError> errores = new ArrayList<TabError>();


    public static boolean error_lexico;                //Error lexico <- Lexico

    /**
     * Inicializa la lista de palabras reservadas
     */
    public AnalizadorSintactico() {
        lista.add("if");
        lista.add("else");
        lista.add("while");
        lista.add("end");
    }

    /**
     * Toma el archivo de entrada y regresa la lista de tokens del analisis lexico
     */
    private void CargaArchivo() {
        AnalizadorLexico lexico = new AnalizadorLexico();
        BufferedReader br;
        FileControl file = new FileControl();
        br = file.initR("entrada.txt");
        tokens = lexico.analiza(br);
    }

    /**
     * Determina la posicion de la lista de la palabra reservada
     *
     * @param ide Token identificador
     * @return La posicion de la lista
     */
    private int Token(String ide) {
        int pos = -1;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(ide)) {
                pos = i;
            }
        }
        return pos;
    }

    /**
     * Comprueba si el tipo recibido es valido o no
     *
     * @param tipo Tipo de token a comprobar
     * @return Nodo de Arbol
     */
    private Arbol comprueba(int tipo) {
        Arbol tmp = new Arbol();
        if (tokens.get(cont).getTipo() == tipo) {
            tmp.AgregarNodo(new Nodo(tokens.get(cont)), 1);
            cont++;
        } else {
            valido = false;
            //Agregamos el error
            errores.add(new TabError(tokens.get(cont).getToken(), tipo, cont));
            //Pasamos pero marcamos el error
            cont++;
        }
        return tmp;
    }

    /**
     * Comprueba si el token que continue es un salto de linea
     **/
    private void comprueba_salto() {
        if (tokens.get(cont).getTipo() == Constantes.SALTO) {
            cont++;
        } else {
            if (tokens.get(cont).getTipo() != Constantes.$) {
                valido = false;
                errores.add(new TabError(tokens.get(cont).getToken(), Constantes.SALTO, cont));
            }
        }
    }

    /**
     * < asignacion >
     *
     * @return
     */
    private Arbol Asignacion() {
        Arbol tmp;
        Nodo aux;

        //< ide >
        tmp = comprueba(Constantes.IDENTIFICADOR);
        aux = tmp.getRaiz();

        //< = >
        if (tokens.get(cont).getTipo() == Constantes.ASIGNACION) {

            //Hacemos raiz el operador y mandamos al hijo izquierdo el ide
            tmp.setRaiz(new Nodo(tokens.get(cont)));
            tmp.AgregarNodo(aux, Constantes.IZQUIERDA);

            //Consumimos el token
            cont++;

            //Agregamos a la raiz la expresion
            tmp.AgregarNodo(Expresion().getRaiz(), Constantes.DERECHA);

            //< del >
            if (tokens.get(cont).getTipo() == Constantes.DELIMITADOR) {
                cont++;
            }

            //< salto >
            comprueba_salto();

        } else {
            valido = false;
        }

        return tmp;
    }

    /**
     * < expresion >
     *
     * @return
     */
    private Arbol Expresion() {
        Arbol tmp = new Arbol();
        Nodo aux;

        //Guardamos al lado izquierdo la expresion.
        tmp.AgregarNodo(Expresion_m().getRaiz(), Constantes.IZQUIERDA);
        //Obtenemos la referencia de la raiz
        aux = tmp.getRaiz();

        //< op_aritmetico1 >
        while (tokens.get(cont).getTipo() == Constantes.OP_ARITMETICOS) {

            //Hacemos raiz el operador y mandamos al hijo izquierdo el ide
            tmp.setRaiz(new Nodo(tokens.get(cont)));
            tmp.AgregarNodo(aux, Constantes.IZQUIERDA);

            //Consumimos el token
            cont++;

            //Guardamos al lado derecho la expresion.
            tmp.AgregarNodo(Expresion_m().getRaiz(), Constantes.DERECHA);
            //Obtenemos la referencia de la raiz
            aux = tmp.getRaiz();
        }
        return tmp;
    }

    /**
     * < expresion_m >
     *
     * @return
     */
    private Arbol Expresion_m() {
        Arbol tmp = new Arbol();
        Nodo aux;

        //Guardamos al lado izquierdo la expresion.
        tmp.AgregarNodo(Expresion_f().getRaiz(), Constantes.IZQUIERDA);
        //Obtenemos la referencia de la raiz
        aux = tmp.getRaiz();

        //<op_aritmetico2 >
        while (tokens.get(cont).getTipo() == Constantes.OP_ARITMETICOS_2) {

            //Hacemos raiz el operador y mandamos al hijo izquierdo el ide
            tmp.setRaiz(new Nodo(tokens.get(cont)));
            tmp.AgregarNodo(aux, Constantes.IZQUIERDA);

            //Consumimos el token
            cont++;

            //Guardamos al lado derecho la expresion.
            tmp.AgregarNodo(Expresion_f().getRaiz(), Constantes.DERECHA);
            //Obtenemos la referencia de la raiz
            aux = tmp.getRaiz();
        }
        return tmp;
    }

    /**
     * < expresion_f >
     *
     * @return
     */
    private Arbol Expresion_f() {
        Arbol tmp = new Arbol();

        //< ide >| < entero >| < flotante >
        if (tokens.get(cont).getTipo() == Constantes.IDENTIFICADOR ||
                tokens.get(cont).getTipo() == Constantes.INT ||
                tokens.get(cont).getTipo() == Constantes.FLOAT) {

            //Agregamos un nodo al arbol
            tmp = new Arbol(tokens.get(cont));

            //Consumimos el token
            cont++;

        } else {
            valido = false;
            // FIXME: 30/04/16
            //Agregamos el error
            errores.add(new TabError(tokens.get(cont).getToken(), Constantes.INT, cont));
            //Pasamos pero marcamos el error
            //cont++;
        }
        return tmp;
    }

    /**
     * < condicion >
     *
     * @return
     */
    private Arbol Condicion() {
        Arbol tmp;
        Nodo aux;

        //Guadamos el arbol
        tmp = Condicion_p();

        //Obtenemos la referencia de la raiz
        aux = tmp.getRaiz();

        //Hacemos raiz el operador y mandamos al hijo izquierdo el
        tmp.setRaiz(comprueba(Constantes.OP_RELACIONAL).getRaiz());
        tmp.AgregarNodo(aux, Constantes.IZQUIERDA);

        //Guardamos al lado derecho la condicion.
        tmp.AgregarNodo(Condicion_p().getRaiz(), Constantes.DERECHA);

        return tmp;
    }

    /**
     * < condicion_p >
     *
     * @return
     */
    private Arbol Condicion_p() {
        Arbol tmp;

        //Agregamos el nodo raiz
        tmp = new Arbol(Expresion().getRaiz());
        return tmp;
    }

    /**
     * < mientras >
     *
     * @return
     */
    private Arbol Mientras() {
        Arbol tmp;

        //Guadamos el arbol
        //< mientras >
        tmp = comprueba(Constantes.IDENTIFICADOR);

        //Guardamos al lado izquierdo del while.
        //< condicion >
        tmp.AgregarNodo(Condicion().getRaiz(), Constantes.IZQUIERDA);

        //comprueba salto
        comprueba_salto();

        //Guardamos al lado derecho del while.
        //< sentencia >
        tmp.AgregarNodo(Sentencia().getRaiz(), Constantes.DERECHA);

        //< end >
        if (Token(tokens.get(cont).getToken()) == Constantes.END) {
            //Consumimos el token
            cont++;
        }

        return tmp;
    }

    /**
     * < si >
     *
     * @return
     */
    private Arbol Si() {
        Arbol tmp;

        //Guadamos el arbol
        //< if >
        tmp = comprueba(Constantes.IDENTIFICADOR);

        //Guardamos al lado izquierdo del if.
        //< Condicion >
        tmp.AgregarNodo(Condicion().getRaiz(), Constantes.IZQUIERDA);

        //Comprueba salto
        //comprueba(Constantes.SALTO);
        comprueba_salto();

        //Guardamos al lado derecho del if.
        //< sentencia >
        tmp.AgregarNodo(Sentencia().getRaiz(), Constantes.DERECHA);

        //< else >
        if (Token(tokens.get(cont).getToken()) == Constantes.IDENTIFICADOR) {
            valido = true;

            //Agregamos el nodo del else
            tmp.AgregarNodo(new Nodo(tokens.get(cont)), Constantes.DERECHA);

            //Consumimos el token
            cont++;

            //comprueba(Constantes.SALTO);
            comprueba_salto();

            //< sentencia >
            //Guardamos al lado derecho del else
            tmp.AgregarNodo(Sentencia().getRaiz(), Constantes.DERECHA);
        }

        //< end >
        if (Token(tokens.get(cont).getToken()) == Constantes.END) {
            //Consumimos el token
            cont++;
        }
        return tmp;
    }

    /**
     * < sentencia >
     *
     * @return
     */
    private Arbol Sentencia() {
        Arbol tmp = new Arbol();
        Nodo aux;

        while (Token(tokens.get(cont).getToken()) != Constantes.END && valido) {
            if (tokens.get(cont).getTipo() == Constantes.$) {
                valido = false;
            } else {
                //Obtenemos la referenica de la raiz
                aux = tmp.getRaiz();
                switch (Token(tokens.get(cont).getToken())) {
                    //verifica que el token en turno no sea una palabra reservada
                    case Constantes.IF:
                        //Hacemos raiz el operador y mandamos al hijo izquierdo el if
                        //< si >
                        tmp.setRaiz(Si().getRaiz());
                        tmp.AgregarNodo(aux, Constantes.IZQUIERDA);
                        comprueba_salto();
                        break;
                    case Constantes.ELSE:
                        valido = false;
                        break;
                    case Constantes.WHILE:
                        //Hacemos raiz el operador y mandamos al hijo izquierdo el while
                        //< mientras >
                        tmp.setRaiz(Mientras().getRaiz());
                        tmp.AgregarNodo(aux, Constantes.IZQUIERDA);
                        comprueba_salto();
                        break;
                    case Constantes.END:
                        break;
                    default:
                        //Hacemos raiz el operador y mandamos al hijo derecho el asignacion
                        // < asignacion >
                        tmp.AgregarNodo(Asignacion().getRaiz(), 2);

                        break;
                }
            }
        }
        return tmp;
    }

    private Arbol Init() {
        Arbol tmp = new Arbol();

        switch (Token(tokens.get(cont).getToken())) {
            //verifica que el token en turno no sea una palabra reservada
            case Constantes.IF:
                //Regla que produce if/if else
                tmp.setRaiz(Si().getRaiz());
                break;
            case Constantes.ELSE:
                //Instruccion else...no deberia entrar a este caso.
                valido = false;
                break;
            case Constantes.WHILE:
                //Regla que produce while
                tmp.setRaiz(Mientras().getRaiz());
                break;
            case Constantes.END:
                //Intruccion end ... no deberia entrar a este caso.
                valido = false;
                break;
            default:
                if (tokens.get(cont).getTipo() == Constantes.IDENTIFICADOR) {
                    tmp.AgregarNodo(Asignacion().getRaiz(), Constantes.DERECHA);
                } else {
                    tmp.AgregarNodo(Expresion().getRaiz(), Constantes.DERECHA);
                }
                break;
        }
        return tmp;
    }

    public boolean analiza() {
        Arbol arbolSintactico = new Arbol();
        CargaArchivo();


        //Tokens
        /*for (int i = 0; i < tokens.size(); i++) {
            System.out.print(tokens.get(i).getToken() + " ");
        }
        System.out.println();*/


        if (!error_lexico) {
            System.out.println("Lexico: OK");
            while (valido && cont < tokens.size() - 1) {
                arbolSintactico.AgregarNodo(Init().getRaiz(), Constantes.DERECHA);
            }
            // FIXME: 01/05/16
            try {
                comprueba(Constantes.$);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        } else {
            System.out.println("Lexico: ERROR");
            valido = false;
        }

        AnalizadorSemantico.arbolSintactico = arbolSintactico;

        // Lista de errores del analisis sintactico
        if (!valido && errores.size() > 0) {
            System.out.println("\n\n****ERRORES analisis sintactico****");
            for (int i = 0; i < errores.size(); i++) {
                System.out.println("Error con el token " +
                        errores.get(i).getPos_token() +
                        ": '" + errores.get(i).getConcepto() +
                        "' se esperaba un " + errores.get(i).getTipo_esperado());
            }
            System.out.println();

        }

        //Recorridos

        /*if (valido) {
            System.out.println("\nInorden");
            arbolSintactico.imprimirEntre(arbolSintactico.getRaiz());
            System.out.println("\nPostorde");
            arbolSintactico.imprimirPost(arbolSintactico.getRaiz());
            System.out.println("\nPreorden");
            arbolSintactico.imprimirPre(arbolSintactico.getRaiz());
            System.out.println();
        }*/
        return valido;
    }
}
