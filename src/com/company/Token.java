package com.company;

/**
 * Created by alejandrobarba on 17/11/15.
 */
public class Token {
    private String token;
    private int tipo;

    public Token(String token, int tipo){
        this.token = token;
        this.tipo = tipo;
    }

    public String getToken(){
        return token;
    }

    public int getTipo(){
        return tipo;
    }
}
