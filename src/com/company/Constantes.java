package com.company;

/**
 * Created by alejandrobarba on 30/04/16.
 */
public class Constantes {
    public static final int ESPACIO = 0;
    public static final int IDENTIFICADOR = 1;
    public static final int INT = 2;
    public static final int FLOAT = 3;
    public static final int OP_ARITMETICOS = 4;
    public static final int DELIMITADOR = 5;
    public static final int OP_ARITMETICOS_2 = 12;
    public static final int OP_RELACIONAL = 9;
    public static final int ASIGNACION = 10;
    public static final int OP_LOGICO = 11;
    public static final int SALTO = 13;

    public static final int DERECHA = 2;
    public static final int IZQUIERDA = 1;

    public static final int IF = 0;
    public static final int ELSE = 1;
    public static final int WHILE = 2;
    public static final int END = 3;

    public static final int $ = -1;

    public static final int NO_REFERENCIA = -200;

}
