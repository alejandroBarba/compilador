package com.company;

/**
 * Created by alejandrobarba on 19/11/15.
 */
public class EstadoValido {

    private int fila;
    private int columna;
    private int tipo;

    public EstadoValido(int fila, int columna, int tipo){
        this.fila = fila;
        this.columna = columna;
        this.tipo = tipo;
    }

    public int getFila(){return fila;}

    public int getColumna(){return columna;}

    public int getTipo(){return tipo;}
}
