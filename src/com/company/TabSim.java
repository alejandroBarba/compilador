package com.company;

/**
 * Created by alejandrobarba on 24/11/15.
 */
public class TabSim {

    private Token token;
    private int valor;

    public TabSim() {

    }

    public TabSim(Token token, int valor) {
        this.token = token;
        this.valor = valor;
    }

    public TabSim(Token token) {
        this.token = token;
    }

    public TabSim(Token token, float valor) {
        this.token = token;
        this.valor = (int) Math.round(valor);
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public void setValor(float valor) {
        this.valor = (int) Math.round(valor);
    }

    public Token getToken() {
        return this.token;
    }

    public int getValor() {
        return this.valor;
    }

}
